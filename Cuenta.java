/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.ArrayList;

/**
 *
 * @author Matías
 */
public class Cuenta {

    private int numCuenta;
    private int numCliente;
    private String tipoCuenta;
    private double saldo;
    private Tarjeta tarjeta;

    ArrayList<Transacción> arrayTrans;

    public Cuenta(int numCuenta, int numCliente, String tipoCuenta, double saldo, Tarjeta tarjeta) {
        this.numCuenta = numCuenta;
        this.numCliente = numCliente;
        this.tipoCuenta = tipoCuenta;
        this.saldo = saldo;
        this.tarjeta = tarjeta;
        this.arrayTrans = new ArrayList<>();
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public ArrayList<Transacción> getArrayTrans() {
        return arrayTrans;
    }

    public void setArrayTrans(ArrayList<Transacción> arrayTrans) {
        this.arrayTrans = arrayTrans;
    }
    public void addArrayTrans(Transacción t){
       arrayTrans.add(t);
    }

    public void consultarCuenta() {

    }
    @Override
    public String toString() {
        return "Cuenta{" + "numCuenta=" + numCuenta + ", numCliente=" + numCliente + ", TipoDeCuenta=" + tipoCuenta + ", Saldo=" + saldo +  ", tarjeta=" + tarjeta + '}';
    }

}
