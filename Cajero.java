/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.ArrayList;

/**
 *
 * @author Matías
 */
public class Cajero {

    private String clave;
    ArrayList<Transacción> arrayTrans;

    public Cajero(String clave) {
        this.clave = clave;
        this.arrayTrans = new ArrayList<>();
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public ArrayList<Transacción> getArrayTrans() {
        return arrayTrans;
    }

    public void setArrayTrans(ArrayList<Transacción> ArrayTrans) {
        this.arrayTrans = ArrayTrans;
    }

    public void addArrayTrans(Transacción x) {
        this.arrayTrans.add(x);
    }

    public void mostrarOpciones() {
    }

    public void solicitarClave() {
    }

    public void verificarBanco() {
    }

    public void darRespuesta() {
    }

    @Override
    public String toString() {
        return "Cajero{" + "clave=" + clave + ", transaccion=" + arrayTrans + '}';
    }
}
