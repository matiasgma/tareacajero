
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.ArrayList;
import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import java.io.FileWriter;
import java.io.IOException;
import javax.xml.transform.TransformerConfigurationException;

/**
 *
 * @author Matías
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws TransformerConfigurationException, TransformerException, ParserConfigurationException {

        Cliente cl1 = new Cliente("juan", 190857441, 1, "Osorno", 76098241);
        Tarjeta t1 = new Tarjeta(23, 432, 3443, null);
        Cuenta c1 = new Cuenta(12314, 214, "corriente", 142214, t1);
        cl1.agregarCuenta(c1);
        cl1.getCuentas().get(0).addArrayTrans(new Transacción(null, "A", "Crédito", 300));

        Cliente cl2 = new Cliente("Pedro", 190857464, 2, "Temuco", 76098242);
        Tarjeta t2 = new Tarjeta(23, 432, 3443, null);
        Cuenta c2 = new Cuenta(12314, 214, "ahorro", 142214, t2);
        cl2.agregarCuenta(c2);
        cl2.getCuentas().get(0).addArrayTrans(new Transacción(null, "B", "Crédito", 300));

        Cliente cl3 = new Cliente("Diego", 190857432, 3, "Puerto Montt", 76098243);
        Tarjeta t3 = new Tarjeta(23, 432, 3443, null);
        Cuenta c3 = new Cuenta(12314, 214, "corriente", 142214, t3);
        cl3.agregarCuenta(c3);
        cl3.getCuentas().get(0).addArrayTrans(new Transacción(null, "C", "Crédito", 300));

        Cliente cl4 = new Cliente("Matias", 190857472, 4, "Valdivia", 76098244);
        Tarjeta t4 = new Tarjeta(23, 432, 3443, null);
        Cuenta c4 = new Cuenta(12314, 214, "ahorro", 142214, t4);
        cl4.agregarCuenta(c4);
        cl4.getCuentas().get(0).addArrayTrans(new Transacción(null, "D", "Crédito", 300));

        Cliente cl5 = new Cliente("Esteban", 190857478, 5, "Curicó", 76098245);
        Tarjeta t5 = new Tarjeta(23, 432, 3443, null);
        Cuenta c5 = new Cuenta(12314, 214, "corriente", 142214, t5);
        cl5.agregarCuenta(c5);
        cl5.getCuentas().get(0).addArrayTrans(new Transacción(null, "E", "Crédito", 300));

        Cliente cl6 = new Cliente("Ariel", 190857465, 6, "Santiago", 76098246);
        Tarjeta t6 = new Tarjeta(23, 432, 3443, null);
        Cuenta c6 = new Cuenta(12314, 214, "ahorro", 142214, t6);
        cl6.agregarCuenta(c6);
        cl6.getCuentas().get(0).addArrayTrans(new Transacción(null, "F", "Crédito", 300));

        Cliente cl7 = new Cliente("Nicolas", 190857442, 7, "La Serena", 76098247);
        Tarjeta t7 = new Tarjeta(23, 432, 3443, null);
        Cuenta c7 = new Cuenta(12314, 214, "corriente", 142214, t7);
        cl7.agregarCuenta(c7);
        cl7.getCuentas().get(0).addArrayTrans(new Transacción(null, "G", "Crédito", 300));

        Cliente cl8 = new Cliente("María", 190857443, 8, "Calama", 76098248);
        Tarjeta t8 = new Tarjeta(23, 432, 3443, null);
        Cuenta c8 = new Cuenta(12314, 214, "ahorro", 142214, t8);
        cl8.agregarCuenta(c8);
        cl8.getCuentas().get(0).addArrayTrans(new Transacción(null, "H", "Crédito", 300));

        Cliente cl9 = new Cliente("Jorge", 190857444, 9, "Loa Andes", 76098249);
        Tarjeta t9 = new Tarjeta(23, 432, 3443, null);
        Cuenta c9 = new Cuenta(12314, 214, "corriente", 142214, t9);
        cl9.agregarCuenta(c9);
        cl9.getCuentas().get(0).addArrayTrans(new Transacción(null, "I", "Crédito", 300));

        Cliente cl10 = new Cliente("Nicole", 190857445, 10, "Rancagua", 60982410);
        Tarjeta t10 = new Tarjeta(23, 432, 3443, null);
        Cuenta c10 = new Cuenta(12314, 214, "ahorro", 142214, t10);
        cl10.agregarCuenta(c10);
        cl10.getCuentas().get(0).addArrayTrans(new Transacción(null, "J", "Crédito", 300));

        Cliente cl11 = new Cliente("Sebastian", 190857446, 11, "Coquimbo", 60982411);
        Tarjeta t11 = new Tarjeta(23, 432, 3443, null);
        Cuenta c11 = new Cuenta(12314, 214, "corriente", 142214, t11);
        cl11.agregarCuenta(c11);
        cl11.getCuentas().get(0).addArrayTrans(new Transacción(null, "K", "Crédito", 300));

        Cliente cl12 = new Cliente("Felipe", 190857442, 12, "San Fernando", 60982412);
        Tarjeta t12 = new Tarjeta(23, 432, 3443, null);
        Cuenta c12 = new Cuenta(12314, 214, "ahorro", 142214, t12);
        cl12.agregarCuenta(c12);
        cl12.getCuentas().get(0).addArrayTrans(new Transacción(null, "L", "Crédito", 300));

        Cliente cl13 = new Cliente("Francisco", 220857441, 13, "Puerto Varas", 60982413);
        Tarjeta t13 = new Tarjeta(23, 432, 3443, null);
        Cuenta c13 = new Cuenta(12314, 214, "corriente", 142214, t13);
        cl13.agregarCuenta(c13);
        cl13.getCuentas().get(0).addArrayTrans(new Transacción(null, "M", "Crédito", 300));

        Cliente cl14 = new Cliente("Camilo", 210857441, 14, "Castro", 60982414);
        Tarjeta t14 = new Tarjeta(23, 432, 3443, null);
        Cuenta c14 = new Cuenta(12314, 214, "ahorro", 142214, t14);
        cl14.agregarCuenta(c14);
        cl14.getCuentas().get(0).addArrayTrans(new Transacción(null, "N", "Crédito", 300));

        Cliente cl15 = new Cliente("Camila", 140857441, 15, "Antofagasta", 60982415);
        Tarjeta t15 = new Tarjeta(23, 432, 3443, null);
        Cuenta c15 = new Cuenta(12314, 214, "corriente", 142214, t15);
        cl15.agregarCuenta(c15);
        cl15.getCuentas().get(0).addArrayTrans(new Transacción(null, "O", "Crédito", 300));

        Cliente cl16 = new Cliente("Luis", 150857441, 16, "Iquique", 60982416);
        Tarjeta t16 = new Tarjeta(23, 432, 3443, null);
        Cuenta c16 = new Cuenta(12314, 214, "ahorro", 142214, t16);
        cl16.agregarCuenta(c16);
        cl16.getCuentas().get(0).addArrayTrans(new Transacción(null, "P", "Crédito", 300));

        Cliente cl17 = new Cliente("Pablo", 160857441, 17, "Valparaíso", 60982417);
        Tarjeta t17 = new Tarjeta(23, 432, 3443, null);
        Cuenta c17 = new Cuenta(12314, 214, "corriente", 142214, t17);
        cl17.agregarCuenta(c17);
        cl17.getCuentas().get(0).addArrayTrans(new Transacción(null, "Q", "Crédito", 300));

        Cliente cl18 = new Cliente("Alvaro", 170857441, 18, "Copiapó", 60982418);
        Tarjeta t18 = new Tarjeta(23, 432, 3443, null);
        Cuenta c18 = new Cuenta(12314, 214, "ahorro", 142214, t18);
        cl18.agregarCuenta(c18);
        cl18.getCuentas().get(0).addArrayTrans(new Transacción(null, "R", "Crédito", 300));

        Cliente cl19 = new Cliente("Marcelo", 180857441, 19, "Victoria", 60982419);
        Tarjeta t19 = new Tarjeta(23, 432, 3443, null);
        Cuenta c19 = new Cuenta(12314, 214, "corriente", 142214, t19);
        cl19.agregarCuenta(c19);
        cl19.getCuentas().get(0).addArrayTrans(new Transacción(null, "S", "Crédito", 300));

        Cliente cl20 = new Cliente("Gabriela", 190857441, 20, "Puyehue", 60982420);
        Tarjeta t20 = new Tarjeta(23, 432, 3443, null);
        Cuenta c20 = new Cuenta(12314, 214, "ahorro", 142214, t20);
        cl20.agregarCuenta(c20);
        cl20.getCuentas().get(0).addArrayTrans(new Transacción(null, "T", "Crédito", 300));

        ArrayList<Cliente> ac = new ArrayList();

        ac.add(cl1);
        ac.add(cl2);
        ac.add(cl3);
        ac.add(cl4);
        ac.add(cl5);
        ac.add(cl6);
        ac.add(cl7);
        ac.add(cl8);
        ac.add(cl9);
        ac.add(cl10);
        ac.add(cl11);
        ac.add(cl12);
        ac.add(cl13);
        ac.add(cl14);
        ac.add(cl15);
        ac.add(cl16);
        ac.add(cl17);
        ac.add(cl18);
        ac.add(cl19);
        ac.add(cl20);

        Banco b1 = new Banco(1, "Banco Santander", "Temuco", 642567);
        Banco b2 = new Banco(2, "Banco Estado", "Temuco", 989445);
        Banco b3 = new Banco(3, "Banco Edwards", "Temuco", 345078);

        b1.addCajero(new Cajero("1234"));
        b1.addCajero(new Cajero("4321"));
        b2.addCajero(new Cajero("2314"));
        b3.addCajero(new Cajero("4123"));

        ArrayList<Banco> banco = new ArrayList();
        banco.add(b1);
        banco.add(b2);
        banco.add(b3);

        // Asignacion cuentas clientes a cuentas banco
        for (int i = 0; i < ac.size(); i++) {
            for (int j = 0; j < ac.get(i).getCuentas().size(); j++) {
                if (i < 10) {
                    b1.addCuenta(ac.get(i).getCuentas().get(j));
                } else if (i < 15 && i > 9) {
                    b2.addCuenta(ac.get(i).getCuentas().get(j));
                } else {
                    b3.addCuenta(ac.get(i).getCuentas().get(j));
                }
            }
        }
        // Asignacion transacciones cliente a transacciones cajero
        for (int i = 0; i < ac.size(); i++) {
            for (int j = 0; j < ac.get(i).getCuentas().size(); j++) {
                for (int k = 0; k < ac.get(i).getCuentas().get(j).getArrayTrans().size(); k++) {
                    if (i < 5) {
                        b1.getCajeros().get(0).addArrayTrans(ac.get(i).getCuentas().get(j).getArrayTrans().get(k));
                    } else if (i < 10) {
                        b1.getCajeros().get(1).addArrayTrans(ac.get(i).getCuentas().get(j).getArrayTrans().get(k));
                    } else if (i < 15 && i > 9) {
                        b2.getCajeros().get(0).addArrayTrans(ac.get(i).getCuentas().get(j).getArrayTrans().get(k));
                    } else {
                        b3.getCajeros().get(0).addArrayTrans(ac.get(i).getCuentas().get(j).getArrayTrans().get(k));
                    }
                }
            }
        }

        //XML ArrayList Clientes
        DocumentBuilderFactory docFact = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFact.newDocumentBuilder();

        // root elements
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("Clientes");
        doc.appendChild(rootElement);

        for (int i = 0; i < ac.size(); i++) {

            Element Cliente = doc.createElement("Cliente");
            rootElement.appendChild(Cliente);

            Element nombre = doc.createElement("Nombre");
            nombre.appendChild(doc.createTextNode("" + ac.get(i).getNombre()));
            Cliente.appendChild(nombre);

            Element direccion = doc.createElement("Dirección");
            direccion.appendChild(doc.createTextNode("" + ac.get(i).getDireccion()));
            Cliente.appendChild(direccion);

            Element identificacion = doc.createElement("Identificación");
            identificacion.appendChild(doc.createTextNode("" + ac.get(i).getIdentificacion()));
            Cliente.appendChild(identificacion);
            
            Element numCliente = doc.createElement("numCliente");
            numCliente.appendChild(doc.createTextNode("" + ac.get(i).getNumCliente()));
            Cliente.appendChild(numCliente);
            
            

            Element telefono = doc.createElement("Teléfono");
            telefono.appendChild(doc.createTextNode("" + ac.get(i).getTelefono()));
            Cliente.appendChild(telefono);

            for (int j = 0; j < ac.get(i).getCuentas().size(); j++) {

                Element cuenta = doc.createElement("Cuenta");
                Cliente.appendChild(cuenta);

                Element numcuenta = doc.createElement("numcuenta");
                numcuenta.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getNumCuenta() + ""));
                cuenta.appendChild(numcuenta);

                Element numcliente = doc.createElement("numcliente");
                numcliente.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getNumCliente() + ""));
                cuenta.appendChild(numcliente);

                Element tipocuenta = doc.createElement("tipocuenta");
                tipocuenta.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getTipoCuenta() + ""));
                cuenta.appendChild(tipocuenta);

                Element saldo = doc.createElement("Saldo");
                saldo.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getSaldo() + ""));
                cuenta.appendChild(saldo);

                Element tarjeta = doc.createElement("Tarjeta");
                cuenta.appendChild(tarjeta);

                Element numerotarjeta = doc.createElement("númeroTarjeta");
                numerotarjeta.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getTarjeta().getNumTarjeta() + ""));
                tarjeta.appendChild(numerotarjeta);

                Element numerocuenta = doc.createElement("númeroCuenta");
                numerocuenta.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getTarjeta().getNumCuenta() + ""));
                tarjeta.appendChild(numerocuenta);

                Element numerocliente = doc.createElement("númeroCliente");
                numerocliente.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getTarjeta().getNumCliente() + ""));
                tarjeta.appendChild(numerocliente);

                Element fechaexpiracion = doc.createElement("fechaExpiración");
                fechaexpiracion.appendChild(doc.createTextNode(ac.get(i).getCuentas().get(j).getTarjeta().getFechaExpiracion() + ""));
                tarjeta.appendChild(fechaexpiracion);

            }
        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(new File("C:\\Users\\Matías\\Desktop\\Cajero1\\src\\cajero1\\datoscliente.xml"));
        transformer.transform(source, result);

        System.out.println("File saved!");

        // XML ArrayList Transacciones
        Document doc2 = docBuilder.newDocument();
        Element raiz = doc2.createElement("Transacciones");
        doc2.appendChild(raiz);
        int cont = 0;
        ArrayList<Object> transac = new ArrayList();
        for (int i = 0; i < ac.size(); i++) {
            for (int j = 0; j < ac.get(i).getCuentas().size(); j++) {
                for (int k = 0; k < ac.get(i).getCuentas().get(j).getArrayTrans().size(); k++) {
                    for (int l = 0; l < banco.size(); l++) {
                        for (int m = 0; m < banco.get(l).getCajeros().size(); m++) {
                            for (int n = 0; n < banco.get(l).getCajeros().get(m).getArrayTrans().size(); n++) {
                                if (ac.get(i).getCuentas().get(j).getArrayTrans().get(k).equals(banco.get(l).getCajeros().get(m).getArrayTrans().get(n))) {
                                    cont++;
                                    ArrayList<Object> s = new ArrayList();
                                    s.add(new Transacción(banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getFecha(), banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getDescripcion(),
                                            banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getTipo(), banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getValor()));
                                    s.add(new Cajero(banco.get(l).getCajeros().get(m).getClave()));
                                    s.add(new Cuenta(ac.get(i).getCuentas().get(j).getNumCuenta(), ac.get(i).getCuentas().get(j).getNumCliente(),
                                            ac.get(i).getCuentas().get(j).getTipoCuenta(), ac.get(i).getCuentas().get(j).getSaldo(),
                                            ac.get(i).getCuentas().get(j).getTarjeta()));
                                    s.add(new Cliente(ac.get(i).getNombre(), ac.get(i).getIdentificacion(), ac.get(i).getNumCliente(), ac.get(i).getDireccion(), ac.get(i).getTelefono()));
                                    transac.add(s);
                                    Element trans = doc2.createElement("Transacción");
                                    raiz.appendChild(trans);
                                    
                                    Element fecha = doc2.createElement("Fecha");
                                    fecha.appendChild(doc2.createTextNode(""+banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getFecha()));
                                    trans.appendChild(fecha);

                                    Element des = doc2.createElement("Descripción");
                                    des.appendChild(doc2.createTextNode(banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getDescripcion()));
                                    trans.appendChild(des);

                                    Element tipo = doc2.createElement("Tipo");
                                    tipo.appendChild(doc2.createTextNode(banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getTipo()));
                                    trans.appendChild(tipo);

                                    Element valor = doc2.createElement("Valor");
                                    valor.appendChild(doc2.createTextNode("" + banco.get(l).getCajeros().get(m).getArrayTrans().get(n).getValor()));
                                    trans.appendChild(valor);

                                    Element cajero = doc2.createElement("Cajero");
                                    cajero.appendChild(doc2.createTextNode(banco.get(l).getCajeros().get(m).getClave()));
                                    trans.appendChild(cajero);

                                    Element cuenta = doc2.createElement("Cuenta");
                                    trans.appendChild(cuenta);

                                    Element numCuenta = doc2.createElement("numCuenta");
                                    numCuenta.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getNumCuenta()));
                                    cuenta.appendChild(numCuenta);

                                    Element numCliente = doc2.createElement("numCliente");
                                    numCliente.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getNumCliente()));
                                    cuenta.appendChild(numCliente);

                                    Element tipoCuenta = doc2.createElement("tipoCuenta");
                                    tipoCuenta.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getTipoCuenta()));
                                    cuenta.appendChild(tipoCuenta);

                                    Element saldo = doc2.createElement("Saldo");
                                    saldo.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getSaldo()));
                                    cuenta.appendChild(saldo);

                                    Element tarjeta = doc2.createElement("Tarjeta");
                                    trans.appendChild(tarjeta);

                                    Element numTarjeta = doc2.createElement("numTarjeta");
                                    numTarjeta.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getTarjeta().getNumTarjeta()));
                                    tarjeta.appendChild(numTarjeta);

                                    Element numCuentaT = doc2.createElement("numCuenta");
                                    numCuentaT.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getTarjeta().getNumCuenta()));
                                    tarjeta.appendChild(numCuentaT);

                                    Element numClienteT = doc2.createElement("numClienteT");
                                    numClienteT.appendChild(doc2.createTextNode("" + ac.get(i).getCuentas().get(j).getTarjeta().getNumCliente()));
                                    tarjeta.appendChild(numClienteT);

                                    Element cliente = doc2.createElement("Cliente");
                                    trans.appendChild(cliente);

                                    Element nombre = doc2.createElement("Nombre");
                                    nombre.appendChild(doc2.createTextNode(ac.get(i).getNombre()));
                                    cliente.appendChild(nombre);
                                    
                                     Element id = doc2.createElement("Identificación");
                                    id.appendChild(doc2.createTextNode("" + ac.get(i).getIdentificacion()));
                                    cliente.appendChild(id);
                                    
                                    Element numClienteC = doc2.createElement("numCliente");
                                    numClienteC.appendChild(doc2.createTextNode("" + ac.get(i).getNumCliente()));
                                    cliente.appendChild(numClienteC);

                                    Element direccion = doc2.createElement("Dirección");
                                    direccion.appendChild(doc2.createTextNode(ac.get(i).getDireccion()));
                                    cliente.appendChild(direccion);

                                                                     
                                    Element telefono = doc2.createElement("Teléfono");
                                    telefono.appendChild(doc2.createTextNode(""+ac.get(i).getTelefono()));
                                    cliente.appendChild(telefono);

                                    Element ban = doc2.createElement("Banco");
                                    trans.appendChild(ban);
                                    
                                     Element numBanco = doc2.createElement("numBanco");
                                    numBanco.appendChild(doc2.createTextNode("" + banco.get(l).getNumBanco()));
                                    ban.appendChild(numBanco);

                                    Element nombreBanco = doc2.createElement("Nombre");
                                    nombreBanco.appendChild(doc2.createTextNode(banco.get(l).getNombre()));
                                    ban.appendChild(nombreBanco);

                                    Element direccionBanco = doc2.createElement("Dirección");
                                    direccionBanco.appendChild(doc2.createTextNode(banco.get(l).getDireccion()));
                                    ban.appendChild(direccionBanco);

                                   
                                    Element telefonoBanco = doc2.createElement("Teléfono");
                                    telefonoBanco.appendChild(doc2.createTextNode("" + banco.get(l).getTelefono()));
                                    ban.appendChild(telefonoBanco);

                                }
                            }
                        }
                    }
                }
            }
        }

        DOMSource sourc = new DOMSource(doc2);
        StreamResult resul = new StreamResult(new File("C:\\\\Users\\\\Matías\\\\Desktop\\\\Cajero1\\\\src\\\\cajero1\\\\datostransacción.xml"));

        transformer.transform(sourc, resul);

        System.out.println("File saved!");

       //Json Clientes
        Gson gson = new Gson();
        String pars = gson.toJson(ac);
        System.out.println(pars);
        try {
            JsonWriter jw = new JsonWriter(new FileWriter("C:\\Users\\Matías\\Desktop\\Cajero1\\src\\cajero1\\jsonCliente.json"));
            jw.beginObject();
            jw.name("Clientes").jsonValue(pars);
            jw.endObject();
            jw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        
        //Json Transacciones
        try {
            JsonWriter jsw = new JsonWriter(new FileWriter("C:\\Users\\Matías\\Desktop\\Cajero1\\src\\cajero1\\jsonTransacción.json"));
            jsw.beginObject();
            jsw.name("Transacciones").jsonValue(gson.toJson(transac));
            jsw.endObject();
            jsw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

   }
}
