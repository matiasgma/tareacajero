/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.Date;

/**
 *
 * @author Matías
 */
public class Transacción {
    
    private Date fecha;
    private String descripcion;
    private String tipo;
    private double valor;

    public Transacción(Date fecha, String descripcion, String tipo, double valor) {
        this.fecha = fecha;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.valor = valor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    
    public void Registrar(){}
    
     @Override
    public String toString() {
        return "Transaccion{" +"fecha= "+fecha+ "descripcion=" + descripcion + ", tipo=" + tipo + ", valor=" + valor + '}';
    }
    
}
