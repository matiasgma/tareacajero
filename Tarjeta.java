/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.Date;

/**
 *
 * @author Matías
 */
public class Tarjeta {
    
    private int numTarjeta;
    private int numCuenta;
    private int numCliente;
    private Cuenta cuentaTarjeta;
    private Date fechaExpiracion;

    public Tarjeta(int numTarjeta, int numCuenta, int numCliente, Date fechaExpiracion) {
        this.numTarjeta = numTarjeta;
        this.numCuenta = numCuenta;
        this.numCliente = numCliente;
        this.fechaExpiracion = fechaExpiracion;
    }

    public int getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(int numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public int getNumCuenta() {
        return numCuenta;
    }

    public void setNumCuenta(int numCuenta) {
        this.numCuenta = numCuenta;
    }

    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    public Cuenta getCuentaTarjeta() {
        return cuentaTarjeta;
    }

    public void setCuentaTarjeta(Cuenta cuentaTarjeta) {
        this.cuentaTarjeta = cuentaTarjeta;
    }
    
    

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }
    
    
    @Override
    public String toString() {
        return "Tarjeta{" + "Numtarjeta=" + numTarjeta + ", Numcuenta=" + numCuenta + ", Numcliente=" + numCliente + ", cuentaTarjeta=" + cuentaTarjeta + ", fechadeExpiracion=" + fechaExpiracion + '}';
    }
    
}
