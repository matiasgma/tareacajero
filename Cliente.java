/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cajero1;

import java.util.ArrayList;

/**
 *
 * @author Matías
 */
public class Cliente {
    
    private String nombre;
    private int identificacion;
    private int numCliente;
    private String direccion;
    private int telefono;
    
    ArrayList<Cuenta> cuentas;
    ArrayList<Tarjeta> tarjetas;

    public Cliente(String nombre, int identificacion, int numCliente, String direccion, int telefono) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.numCliente = numCliente;
        this.direccion = direccion;
        this.telefono = telefono;
        
        this.cuentas = new ArrayList<>();
        this.tarjetas= new ArrayList<>();
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public int getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(int numCliente) {
        this.numCliente = numCliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public ArrayList<Cuenta> getCuentas() {
        return cuentas;
    }

    public void setCuentas(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public ArrayList<Tarjeta> getTarjetas() {
        return tarjetas;
    }

    public void setTarjetas(ArrayList<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    
    
    public void ingresarClave(){}
    public void elegirOpciones(){}
    public void valorRetirar(){}
    
    public void agregarCuenta(Cuenta t){
     
        this.cuentas.add(t);
        
    }
    @Override
    public String toString() {
        return "Cliente{" + "NumCliente=" + numCliente + ", Identificacion=" + identificacion + ", Nombre=" + nombre + ", Direccion=" + direccion + ", Telefono=" + telefono + ", Cuentacliente=" + cuentas + ", Tarjetacliente=" + tarjetas + '}';
    }
    
    
}
